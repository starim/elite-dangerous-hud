Elite Dangerous HUD
==

A useful status display for game state in Elite Dangerous. Also tags and
re-encodes your screenshots when you take them.

License: AGPLv3 or later

Setup
--

Requires a [Rust compiler](https://www.rust-lang.org/tools/install) to build.

Edit config.sample.toml to use the settings appropriate for your Elite
Dangerous installation and then rename the file config.toml

Don't launch this program until Elite Dangerous is already started and on the
main menu or else this program won't find the right journal file for the
current game session, and won't catch any events.

Development
--

[Official Elite Dangerous API documentation](http://hosting.zaonce.net/community/journal/v31/Journal_Manual_v31.pdf)  
[Elite Dangerous game file unofficial documentation](https://elite-journal.readthedocs.io/en/latest/)  
[EDSM System API V1](https://www.edsm.net/en/api-system-v1)  
[imgui](https://github.com/imgui-rs/imgui-rs)  
