mod config;
mod error;
mod events;
mod files;
mod gui;
mod journal;
mod logger;
mod state;
mod util;

use std::path::Path;

use tracing::info;

use config::Config;
use gui::Gui;

static mut CONFIG: Option<Config> = None;

fn main() {
    logger::init();

    match Config::new_from_file(Path::new("../config.toml")) {
        Ok(config) => {
            info!(%config, "Loaded config");
            unsafe {
                CONFIG = Some(config.clone());
            }
            config
        },
        Err(error) => {
            panic!("Fatal error reading config file: {}", error);
        }
    };

    eframe::run_native(
        "Elite Dangerous HUD",
        Gui::native_options(),
        Box::new(|cc| Box::new(Gui::new(cc))),
    );
}
