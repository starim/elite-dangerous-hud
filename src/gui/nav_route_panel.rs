use eframe::egui::{Align, Layout, ScrollArea, Ui};

use crate::state::State;

pub fn render(ui: &mut Ui, state: &State) {
    ui.vertical_centered(|ui| {
        if let Some(ref nav_route) = state.nav_route {
            ui.heading("Nav Route");
            ui.label(format!("{} jumps remaining", nav_route.route.len()));
            ui.separator();

            ui.with_layout(Layout::top_down_justified(Align::Min), |ui| {
                ScrollArea::vertical().id_source("route-scroll-area").show(ui, |ui| {
                    for (i, star) in nav_route.route.iter().enumerate() {
                        ui.label(
                            format!("{}.\t{}\t\t{}",
                            i + 1,
                            star.star_class,
                            star.star_system,
                            )
                        );
                    }
                });
            });
        } else {
            ui.heading("No route plotted");
        }
    });
}
