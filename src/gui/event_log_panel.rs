use eframe::egui::{Align, Layout, ScrollArea, Ui};

use crate::state::State;

pub fn render(ui: &mut Ui, state: &State) {
    ui.vertical_centered(|ui| {
        ui.heading("Status Display");
        ui.with_layout(Layout::top_down_justified(Align::Min), |ui| {
            ScrollArea::vertical().id_source("log-message-scroll-area").show(ui, |ui| {
                for message in state.event_log.iter() {
                    ui.label(message);
                }
            });
        });
    });
}
