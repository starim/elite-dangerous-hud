mod current_star_system_panel;
mod event_log_panel;
mod nav_route_panel;
mod ship_status_panel;
mod title_bar;

use std::sync::mpsc::{channel, TryRecvError, Receiver};
use std::thread;

use eframe::{App, CreationContext, egui, egui::color::Color32, Frame};

use crate::events::Event;
use crate::journal::Journal;
use crate::state::State;

pub struct Gui {
    state: State,
    event_receiver: Receiver<Event>,
    journal_watch_thread: thread::JoinHandle<()>,
}

impl App for Gui {
    fn update(
        &mut self,
        ctx: &egui::Context,
        _frame: &mut Frame,
    ) {
        match self.event_receiver.try_recv() {
            Ok(event) => event.process(&mut self.state),
            Err(error) => {
                match error {
                    TryRecvError::Empty => (),
                    TryRecvError::Disconnected => panic!("Gui has been disconnected from event channel. The journal watch thread probably panicked. Receiving new journal events is no longer possible."),
                }
            },
        }

        title_bar::render(&ctx, &self.state);

        let stroke_width = 1.0;
        egui::CentralPanel::default()
            .frame(egui::containers::Frame {
                stroke: egui::Stroke {
                    width: stroke_width,
                    color: Color32::from_rgb(255_u8, 111_u8, 0_u8),
                },
                ..egui::containers::Frame::default()
            })
            .show(ctx, |ui| {
                let half_height = ui.available_height()/2.0;
                let half_width = ui.available_width()/2.0 - stroke_width - 4.0;
                egui::Grid::new("main_grid")
                .min_row_height(half_height)
                .min_col_width(half_width)
                .max_col_width(half_width)
                .show(ui, |ui| {
                    current_star_system_panel::render(ui, &self.state);
                    ship_status_panel::render(ui, &self.state);
                    ui.end_row();
                    nav_route_panel::render(ui, &self.state);
                    event_log_panel::render(ui, &self.state);
                    ui.end_row();
                });
            });
    }
}

impl Gui {
    pub fn native_options() -> eframe::NativeOptions {
        eframe::NativeOptions {
            maximized: true,
            decorated: true,
            vsync: true,
            ..eframe::NativeOptions::default()
        }
    }

    pub fn new(cc: &CreationContext<'_>) -> Self {
        let ctx = &cc.egui_ctx;

        let (tx, rx) = channel();
        let context_clone = ctx.clone();
        let context_clone2 = ctx.clone();
        let journal_watch_thread = thread::spawn(move || {
            let mut journal = Journal::new(tx);
            journal.process_initial_entries(Some(context_clone2));
            journal.permanent_watch_loop(context_clone);
        });

        let primary_color = Color32::from_rgb(255_u8, 111_u8, 0_u8);
        let secondary_color = Color32::from_rgb(79_u8, 41_u8, 12_u8);
        let secondary_color_outline = Color32::from_rgba_unmultiplied(79_u8, 41_u8, 12_u8, 128_u8);
        let mut visuals = egui::Visuals::dark();

        let inactive_style = egui::style::WidgetVisuals {
            bg_fill: secondary_color_outline,
            bg_stroke: egui::Stroke {
                width: 0.3,
                color: primary_color,
            },
            rounding: egui::Rounding::default(),
            fg_stroke: egui::Stroke {
                width: 1.0,
                color: primary_color,
            },
            expansion: 0.0,
        };

        let active_style = egui::style::WidgetVisuals {
            bg_fill: primary_color,
            bg_stroke: egui::Stroke {
                width: 0.3,
                color: secondary_color_outline,
            },
            rounding: egui::Rounding::default(),
            fg_stroke: egui::Stroke {
                width: 1.0,
                color: secondary_color,
            },
            expansion: 0.0,
        };

        visuals.widgets.inactive = inactive_style;
        visuals.widgets.hovered = active_style;
        visuals.widgets.active = active_style;
        visuals.widgets.open = active_style;

        let mut fonts = egui::FontDefinitions::default();
        fonts.font_data.insert("Eurostyle".to_owned(),
           egui::FontData::from_static(include_bytes!("../../assets/EurostileExtended.ttf")));
        fonts.families.get_mut(&egui::FontFamily::Proportional).unwrap()
            .insert(0, "Eurostyle".to_owned());
        fonts.families.get_mut(&egui::FontFamily::Monospace).unwrap()
            .push("Eurostyle".to_owned());

        ctx.set_visuals(visuals);
        ctx.set_fonts(fonts);

        Self {
            state: State::default(),
            event_receiver: rx,
            journal_watch_thread: journal_watch_thread,
        }
    }
}
