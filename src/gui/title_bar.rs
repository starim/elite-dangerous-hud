use eframe::egui::{Context, TopBottomPanel};

use crate::state::State;

pub fn render(ctx: &Context, state: &State) {
    TopBottomPanel::top("top panel")
    .default_height(ctx.style().spacing.interact_size.y * 3.0)
    .show(ctx, |ui| {
        ui.centered_and_justified(|ui| {
            ui.heading(format!(
                "\"{}\" {}",
                &*state.ship.name,
                &*state.ship.ident,
            ));
        });
    });
}
