use eframe::egui::Ui;

use crate::state::State;

pub fn render(ui: &mut Ui, state: &State) {
    ui.vertical_centered(|ui| {
        ui.heading(&*state.current_system.star_system_name);
        ui.label(&*state.current_system.star_name);
        if let Some(ref faction) = state.current_system.system_faction {
            if let Some(ref allegiance) = faction.allegiance {
                ui.label(&*allegiance);
            } else {
                ui.label("Unclaimed Space");
            }
            ui.label(&*state.current_system.star_name);
            ui.label(format!(
                "Owned by {} ({})",
                faction.name,
                faction.government.as_deref().unwrap_or("Unknown"),
            ));
            if let Some(ref state) = faction.state {
                ui.label(format!("State: {}", state));
            }
            if let Some(ref station_info) = state.docking_station {
                ui.label(format!("Economy: {}", station_info.station_economy));
            }
            if let Some(ref reputation) = faction.my_reputation {
                ui.label(format!("Reputation: {}", reputation));
            }
        }
    });
}
