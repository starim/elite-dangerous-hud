use eframe::egui::Ui;

use crate::state::State;

pub fn render(ui: &mut Ui, _state: &State) {
    ui.vertical_centered(|ui| {
        ui.heading("Top Right Panel");
        ui.label("contents");
    });
}
