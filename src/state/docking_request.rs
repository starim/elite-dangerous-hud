use crate::events::travel::DockingGrantedPayload;

#[derive(Debug, Clone, PartialEq)]
pub struct DockingRequest {
    pub station_name: String,
    pub station_type: String,
    pub market_id: u64,
    pub landing_pad_number: u32,
}

impl From<DockingGrantedPayload> for DockingRequest {
    fn from(event: DockingGrantedPayload) -> Self {
        DockingRequest {
            station_name: event.station_name,
            station_type: event.station_type,
            market_id: event.market_id,
            landing_pad_number: event.landing_pad,
        }
    }
}
