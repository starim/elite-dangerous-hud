mod conflict;
pub use self::conflict::Conflict;
mod docking_request;
pub use self::docking_request::DockingRequest;
mod docking_station_info;
pub use self::docking_station_info::DockingStationInfo;
mod economy;
pub use self::economy::Economy;
mod faction;
pub use self::faction::Faction;
mod nav_route;
pub use self::nav_route::NavRoute;
pub use self::nav_route::RouteSegment;
mod planetary_touch_down_info;
pub use self::planetary_touch_down_info::PlanetaryTouchDownInfo;
mod ship_info;
pub use self::ship_info::ShipInfo;
mod system_info;
pub use self::system_info::SystemInfo;

use std::collections::VecDeque;

const MAX_EVENT_LOG_LENGTH: usize = 100;

#[derive(Debug, Clone, PartialEq, Default)]
pub struct State {
    pub commander_name: String,
    pub game_mode: String,
    pub ship: ShipInfo,
    pub nav_route: Option<NavRoute>,
    pub event_log: VecDeque<String>,
    pub nearby_body_name: Option<String>,
    pub docking_station: Option<DockingStationInfo>,
    pub active_docking_request: Option<DockingRequest>,
    pub current_system: SystemInfo,
    pub planetary_touch_down: Option<PlanetaryTouchDownInfo>,
}

impl State {
    pub fn log_message<S: Into<String>>(&mut self, message: S) {
        self.event_log.push_front(message.into());
        self.event_log.truncate(MAX_EVENT_LOG_LENGTH);
    }
}
