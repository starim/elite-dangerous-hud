use std::fmt;

use serde::{Serialize, Deserialize};

use super::faction::Faction;

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct Conflict {
    /// War, Civil War, etc.
    pub war_type: String,
    /// Active, etc.
    pub status: String,
    pub faction1: Faction,
    pub faction2: Faction,
}

impl fmt::Display for Conflict {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "{} {} between {} and {}",
            self.status,
            self.war_type.to_ascii_lowercase(),
            self.faction1.name,
            self.faction2.name,
        )
    }
}
