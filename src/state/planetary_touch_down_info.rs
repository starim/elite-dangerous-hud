use std::convert::TryFrom;

use crate::events::travel::TouchdownPayload;

#[derive(Debug, Clone, PartialEq)]
pub struct PlanetaryTouchDownInfo {
    pub body_name: String,
    pub body_id: u32,
    /// only if there's a point of interest within 50km
    pub nearest_destination: Option<String>,
    pub latitude: f32,
    pub longitude: f32,
}

impl TryFrom<TouchdownPayload> for PlanetaryTouchDownInfo {
    type Error = ();

    fn try_from(event: TouchdownPayload) -> Result<Self, Self::Error> {
        if !event.on_planet.unwrap_or(false) || !event.player_controlled {
            return Err(());
        }

        Ok(PlanetaryTouchDownInfo {
            body_name: event.body.ok_or(())?,
            body_id: event.body_id.ok_or(())?,
            nearest_destination: event.nearest_destination,
            latitude: event.latitude.ok_or(())?,
            longitude: event.longitude.ok_or(())?,
        })
    }
}
