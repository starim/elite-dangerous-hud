use std::fmt;

use serde::{Serialize, Deserialize};

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct Economy {
    #[serde(rename = "Name_Localised")]
    pub name: String,
    pub proportion: f32,
}

impl fmt::Display for Economy {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.name)
    }
}
