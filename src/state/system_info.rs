use super::{Conflict, Faction};

use crate::events::travel::{FSDJumpPayload, LocationPayload};

#[derive(Debug, Clone, PartialEq, Default)]
pub struct SystemInfo {
    pub star_system_name: String,
    pub system_address: u64,
    /// three-dimensional coordinates
    pub star_pos: Vec<f32>,
    pub star_name: String,
    pub system_faction: Option<Faction>,
    /// Federation, Empire, Alliance, etc.
    pub system_allegiance: String,
    pub system_economy: String,
    pub system_second_economy: String,
    pub system_government: String,
    /// Anarchy, Medium Security, etc.
    pub system_security: String,
    pub factions: Option<Vec<Faction>>,
    pub population: u64,
    pub conflicts: Option<Vec<Conflict>>,

    /// only Odyssey?
    pub wanted: Option<String>,
}

impl From<FSDJumpPayload> for SystemInfo {
    fn from(event: FSDJumpPayload) -> Self {
        SystemInfo {
            star_system_name: event.star_system,
            system_address: event.system_address,
            star_pos: event.star_pos,
            star_name: event.body,
            system_faction: event.system_faction,
            system_allegiance: event.system_allegiance,
            system_economy: event.system_economy,
            system_second_economy: event.system_second_economy,
            system_government: event.system_government,
            system_security: event.system_security,
            factions: event.factions,
            population: event.population,
            conflicts: event.conflicts,
            wanted: event.wanted,
        }
    }
}

impl From<LocationPayload> for SystemInfo {
    fn from(event: LocationPayload) -> Self {
        SystemInfo {
            star_system_name: event.star_system,
            system_address: event.system_address,
            star_pos: event.star_pos,
            star_name: event.body,
            system_faction: event.system_faction,
            system_allegiance: event.system_allegiance,
            system_economy: event.system_economy,
            system_second_economy: event.system_second_economy,
            system_government: event.system_government,
            system_security: event.system_security,
            factions: event.factions,
            population: event.population,
            conflicts: event.conflicts,
            wanted: event.wanted,
        }
    }
}
