use serde::{Serialize, Deserialize};

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct Faction {
    pub name: String,
    #[serde(rename = "FactionState")]
    pub state: Option<String>,

    // populated by FSDJump event
    /// Cooperative, etc.
    pub government: Option<String>,
    pub influence: Option<f32>,
    /// Independent, etc.
    pub allegiance: Option<String>,
    /// Happy, etc.
    #[serde(rename = "Happiness_Localised")]
    pub happiness: Option<String>,
    pub my_reputation: Option<f32>,

    // populated when getting information about a Conflict
    /// name of location
    pub stake: Option<String>,
    pub won_days: Option<i32>,
}
