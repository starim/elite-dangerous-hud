use std::borrow::Cow;
use std::collections::VecDeque;
use std::fs::read_to_string;

use crate::error::Error;
use serde::{Serialize, Deserialize};

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct NavRoute {
    #[serde(rename = "timestamp")]
    pub timestamp: String,
    pub route: VecDeque<RouteSegment>,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct RouteSegment {
    pub star_system: String,
    /// three-dimensional coordinate
    pub star_pos: Vec<f32>,
    pub star_class: String,
}

impl NavRoute {
    pub fn read() -> Result<NavRoute, Error> {
        let nav_route_path = crate::files::nav_route_file_path();
        let json =
            read_to_string(&nav_route_path)
            .map_err(|error| {
                let message = format!(
                    "Error reading NavRoute.json file ({})",
                    nav_route_path.to_string_lossy(),
                );
                Error::new(Cow::from(message), Box::new(error))
            })?;
        let mut raw_nav_route: NavRoute = serde_json::from_str(&*json).map_err(|error| {
            Error::new("Error parsing NavRoute.json".into(), Box::new(error))
        })?;
        // the journal lists the current system as the first system in the nav route, which we
        // don't want
        raw_nav_route.route.pop_front();
        Ok(raw_nav_route)
    }
}
