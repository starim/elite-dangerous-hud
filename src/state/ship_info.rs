use crate::events::startup::LoadoutPayload;

#[derive(Debug, Clone, PartialEq, Default)]
pub struct ShipInfo {
    pub name: String,
    pub ident: String,
    pub ship_type: String,
    pub unladen_mass: f32,
    pub fuel_level: f32,
    pub fuel_capacity: f32,
    pub cargo_capacity: i32,
    pub max_jump_range: f32,
    pub rebuy: i32,
}

impl From<LoadoutPayload> for ShipInfo {
    fn from(event: LoadoutPayload) -> Self {
        ShipInfo {
            name: event.ship_name,
            ident: event.ship_ident,
            ship_type: crate::util::frontier_name_to_ship_type_display_name(&*event.ship_type_frontier_name).to_owned(),
            unladen_mass: event.unladen_mass,
            fuel_capacity: event.fuel_capacity.main,
            cargo_capacity: event.cargo_capacity,
            max_jump_range: event.max_jump_range,
            rebuy: event.rebuy,
            ..ShipInfo::default()
        }
    }
}
