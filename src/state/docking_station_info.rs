use std::convert::TryFrom;

use crate::events::travel::{DockedPayload, LocationPayload};
use crate::state::Faction;

#[derive(Debug, Clone, PartialEq)]
pub struct DockingStationInfo {
    pub station_name: String,
    /// Outpost, etc.
    pub station_type: String,
    /// star system name
    pub star_system: String,
    pub system_address: u64,
    pub station_faction: Faction,
    pub station_government: String,
    /// Federation, Empire, Alliance, etc.
    pub station_allegiance: Option<String>,
    pub station_services: Vec<String>,
    pub station_economy: String,
    pub market_id: u64,
    pub dist_from_star_ls: Option<f32>,

    // Odyssey only?
    /// whether wanted locally
    pub wanted: Option<bool>,
    pub active_fine: Option<bool>,
}

impl From<DockedPayload> for DockingStationInfo {
    fn from(event: DockedPayload) -> Self {
        DockingStationInfo {
            station_name: event.station_name,
            station_type: event.station_type,
            star_system: event.star_system,
            system_address: event.system_address,
            station_faction: event.station_faction,
            station_government: event.station_government,
            station_allegiance: event.station_allegiance,
            station_services: event.station_services,
            station_economy: event.station_economy,
            market_id: event.market_id,
            dist_from_star_ls: event.dist_from_star_ls,
            wanted: event.wanted,
            active_fine: event.active_fine,
        }
    }
}

impl TryFrom<LocationPayload> for DockingStationInfo {
    type Error = ();

    fn try_from(event: LocationPayload) -> Result<Self, Self::Error> {
        if !event.docked {
            return Err(());
        }

        Ok(DockingStationInfo {
            station_name: event.station_name.ok_or(())?,
            station_type: event.station_type.ok_or(())?,
            star_system: event.star_system,
            system_address: event.system_address,
            station_faction: event.station_faction.ok_or(())?,
            station_government: event.station_government.ok_or(())?,
            station_allegiance: event.station_allegiance,
            station_services: event.station_services.ok_or(())?,
            station_economy: event.station_economy.ok_or(())?,
            market_id: event.market_id.ok_or(())?,
            dist_from_star_ls: event.dist_from_star_ls,
            // not currently supplied by this event, at least according to the documentation
            wanted: None,
            active_fine: None,
        })
    }
}
