static CARGO_FILE: &'static str = "Cargo.json";
static MARKET_FILE: &'static str = "Market.json";
static MODULES_FILE: &'static str = "ModulesInfo.json";
static OUTFITTING_FILE: &'static str = "Outfitting.json";
static SHIPYARD_FILE: &'static str = "Shipyard.json";
static STATUS_FILE: &'static str = "status.json";
static NAV_ROUTE_FILE: &'static str = "NavRoute.json";

use crate::CONFIG;

use std::path::PathBuf;

pub fn game_data_dir_path() -> PathBuf {
    unsafe {
        CONFIG.as_ref().unwrap().game_data_dir()
    }
}

pub fn game_screenshots_dir_path() -> PathBuf {
    unsafe {
        CONFIG.as_ref().unwrap().game_screenshots_dir()
    }
}

pub fn screenshots_target_dir_path() -> PathBuf {
    unsafe {
        CONFIG.as_ref().unwrap().screenshots_target_dir()
    }
}

pub fn nav_route_file_path() -> PathBuf {
    let mut path = game_data_dir_path();
    path.push(NAV_ROUTE_FILE);
    path
}
