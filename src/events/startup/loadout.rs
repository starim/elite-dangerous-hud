use std::borrow::Cow;
use std::fmt;

use serde::{Serialize, Deserialize};
use tracing::info;

use crate::state::{ShipInfo, State};

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct LoadoutPayload {
    #[serde(rename = "timestamp")]
    pub timestamp: String,
    /// internal name for the ship's class (e.g. "empire_eagle" for the Imperial Eagle)
    #[serde(rename(deserialize = "Ship"))]
    pub ship_type_frontier_name: String,
    /// ship ID code (e.g. 34 for Imperial Eagle)
    #[serde(rename(deserialize = "ShipID"))]
    pub ship_id: i32,
    /// the custom name the commander has given their ship
    pub ship_name: String,
    /// the custom ship ID the commander has given their ship (e.g. "BNT-8M")
    pub ship_ident: String,
    /// decimal percent of hull health, with 1.0 as full health
    pub hull_health: f32,
    /// mass of hull and modules but excluding fuel and cargo, in tons
    pub unladen_mass: f32,
    pub fuel_capacity: FuelCapacity,
    pub cargo_capacity: i32,
    /// jump range in Ly with no cargo and just enough fuel for one jump
    pub max_jump_range: f32,
    /// rebuy cost in credits
    pub rebuy: i32,
    /// if wanted
    pub hot: Option<bool>,
    // there's also a list of all modules with very detailed information about engineering effects,
    // power priority, integrity status, ammo, etc.
    // pub modules: Vec<ModuleInfo>,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct FuelCapacity {
    /// main fuel tank
    pub main: f32,
    /// the fuel reservoir in the engine
    pub reserve: f32,
}

impl LoadoutPayload {
    pub fn process(self, state: &mut State) {
        state.ship = ShipInfo::from(self);
    }

    pub fn log(&self) {
        let hot = self.hot.as_ref().map_or(Cow::from("<not specified>"), |value| Cow::from(value.to_string()));

        info!(
            %self.timestamp,
            %self.ship_type_frontier_name,
            %self.ship_id,
            %self.ship_name,
            %self.ship_ident,
            %self.hull_health,
            %self.unladen_mass,
            %self.fuel_capacity,
            %self.cargo_capacity,
            %self.max_jump_range,
            self.rebuy,
            %hot,
            "{}",
            self,
        );
    }
}

impl fmt::Display for LoadoutPayload {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let ship_type = crate::util::frontier_name_to_ship_type_display_name(&*self.ship_type_frontier_name);
        write!(f, "Boarding {} {} \"{}\"", ship_type, self.ship_ident, self.ship_name)
    }
}

impl fmt::Display for FuelCapacity {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "Main fuel tank capacity: {}, engine reservoir: {}", self.main, self.reserve)
    }
}
