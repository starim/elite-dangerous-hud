use std::fmt;

use serde::{Serialize, Deserialize};
use tracing::info;

use crate::state::State;

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct LoadGamePayload {
    #[serde(rename = "timestamp")]
    pub timestamp: String,
    #[serde(rename(deserialize = "FID"))]
    pub player_id: String,
    pub commander: String,
    pub horizons: bool,
    pub odyssey: bool,
    #[serde(rename(deserialize = "Ship"))]
    pub ship_type_code: String,
    #[serde(rename(deserialize = "Ship_Localised"))]
    pub ship_type: String,
    /// ID of the ship's type (e.g. 19 for the Imperial Cutter or 34 for the Imperial Eagle)
    #[serde(rename(deserialize = "ShipID"))]
    pub ship_id: i32,
    /// the custom name the commander has given their ship
    pub ship_name: String,
    /// the custom ship ID the commander has given their ship (e.g. "BNT-8M")
    pub ship_ident: String,
    /// present if the ship is landed
    pub start_landed: Option<bool>,
    /// present if the player is dead
    pub start_dead: Option<bool>,
    /// ship's current fuel level in tons
    pub fuel_level: f32,
    /// ship's fuel capacity in tons
    pub fuel_capacity: f32,
    /// "Open", "Solo", or "Group"
    pub game_mode: String,
    /// if playing in Group game mode, gives the name of the group
    pub group: Option<String>,
    /// player's credit balance
    pub credits: i64,
    /// player's loan amount
    pub loan: i64,
}

impl LoadGamePayload {
    pub fn process(self, state: &mut State) {
        state.commander_name = self.commander.clone();
        state.game_mode = self.game_mode.clone();
    }

    pub fn log(&self) {
        info!(
            %self.timestamp,
            %self.commander,
            %self.horizons,
            %self.odyssey,
            %self.ship_type,
            %self.ship_ident,
            %self.ship_name,
            %self.fuel_level,
            %self.fuel_capacity,
            %self.game_mode,
            %self.credits,
            %self.loan,
            "{}",
            self,
        );
    }
}

impl fmt::Display for LoadGamePayload {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "Spawning in {} mode in \"{}\" {} {}", self.game_mode, self.ship_name, self.ship_ident, self.ship_type)
    }
}
