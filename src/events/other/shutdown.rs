use std::fmt;

use serde::{Serialize, Deserialize};
use tracing::info;

use crate::state::State;

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct ShutdownPayload {
}

impl ShutdownPayload {
    pub fn log(&self) {
        info!("{}", self);
    }

    pub fn process(&self, _state: &mut State) {
        info!("Session ended. Exiting...");
        // TODO: end the session more gracefully
        std::process::exit(0);
    }
}

impl fmt::Display for ShutdownPayload {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "Game shut down")
    }
}
