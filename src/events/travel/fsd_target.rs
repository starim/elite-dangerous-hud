use std::borrow::Cow;
use std::fmt;

use serde::{Serialize, Deserialize};
use tracing::info;

use crate::state::State;

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct FSDTargetPayload {
    #[serde(rename = "timestamp")]
    pub timestamp: String,
    pub name: String,

    /// will only be available for hyperspace jumps
    pub remaining_jumps_in_route: Option<i32>,
    /// K, G, B, F, etc.
    pub star_class: Option<String>,
}

impl FSDTargetPayload {
    pub fn process(self, _state: &mut State) {}

    pub fn log(&self) {
        let remaining_jumps_in_route =
            self.remaining_jumps_in_route
            .map_or(Cow::from("<not specified>"), |value| Cow::from(value.to_string()));
        let star_class =
            self.star_class
            .as_ref()
            .map_or("<not specified>", |value| &*value);

        info!(
            %self.timestamp,
            %self.name,
            %remaining_jumps_in_route,
            %star_class,
            "{}",
            self,
        );
    }
}

impl fmt::Display for FSDTargetPayload {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let target: Cow<str> = if let Some(ref star_class) = self.star_class {
            let remaining_jumps_text: Cow<str> = if let Some(remaining_jumps_in_route) = self.remaining_jumps_in_route {
                format!(" ({} jumps in route)", remaining_jumps_in_route).into()
            } else {
                "".into()
            };

            format!("{} class star {}{}", star_class, self.name, remaining_jumps_text).into()

        } else {
            (&*self.name).into()
        };

        write!(f, "Set FSD target to {}", target)
    }
}
