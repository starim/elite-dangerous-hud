use std::borrow::Cow;
use std::fmt;

use serde::{Serialize, Deserialize};
use tracing::info;

use crate::state::{DockingStationInfo, Faction, State};

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct DockedPayload {
    #[serde(rename = "timestamp")]
    pub timestamp: String,
    pub station_name: String,
    /// Outpost, etc.
    pub station_type: String,
    /// star system name
    pub star_system: String,
    pub system_address: u64,
    pub station_faction: Faction,
    #[serde(rename = "StationGovernment_Localised")]
    pub station_government: String,
    /// Federation, Empire, Alliance, etc.
    pub station_allegiance: Option<String>,
    pub station_services: Vec<String>,
    #[serde(rename = "StationEconomy_Localised")]
    pub station_economy: String,
    #[serde(rename = "MarketID")]
    pub market_id: u64,
    pub cockpit_breach: Option<bool>,
    #[serde(rename = "DistFromStarLS")]
    pub dist_from_star_ls: Option<f32>,

    // Odyssey only?
    /// whether wanted locally
    pub wanted: Option<bool>,
    pub active_fine: Option<bool>,
}

impl DockedPayload {
    pub fn process(self, state: &mut State) {
        state.docking_station = Some(DockingStationInfo::from(self));
    }

    pub fn log(&self) {
        let cockpit_breach =
            self.cockpit_breach
            .map_or(Cow::from("<not specified>"), |value| Cow::from(value.to_string()));
        let dist_from_star_ls =
            self.dist_from_star_ls
            .map_or(Cow::from("<not specified>"), |value| Cow::from(format!("{:.2} Ls", value)));
        let wanted =
            self.wanted
            .map_or(Cow::from("<not specified>"), |value| Cow::from(value.to_string()));
        let active_fine =
            self.active_fine
            .map_or(Cow::from("<not specified>"), |value| Cow::from(value.to_string()));

        info!(
            %self.timestamp,
            %self.station_name,
            %self.station_type,
            %self.star_system,
            self.system_address,
            ?self.station_faction,
            %self.station_government,
            station_allegiance = self.station_allegiance.as_deref().unwrap_or("<None>"),
            station_services = %self.station_services.join(", "),
            %self.station_economy,
            self.market_id,
            %cockpit_breach,
            %dist_from_star_ls,
            %wanted,
            %active_fine,
            "{}",
            self,
        );
    }

    /// prettifies the station type (e.g. "CraterOutpost" => "crater outpost")
    pub fn station_type_pretty(&self) -> &str {
        match &*self.station_type {
            "Coriolis" => "Coliolis station",
            "Orbis" => "Orbis station",
            "Ocellus" => "Ocellus station",
            "CraterOutpost" => "crater outpost",
            _ => &*self.station_type,
        }
    }
}

impl fmt::Display for DockedPayload {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let owning_power_adjective: Cow<str> = if let Some(ref station_allegiance_string) = self.station_allegiance {
            match station_allegiance_string.as_str() {
                "Empire" => " imperial".into(),
                "Federation" => " Federation".into(),
                "Alliance" => " Alliance".into(),
                _ => station_allegiance_string.to_ascii_lowercase().into(),
            }
        } else {
            "".into()
        };

        write!(
            f,
            "Docked at{} {} {} in {}",
            owning_power_adjective,
            self.station_type_pretty(),
            self.station_name,
            self.star_system,
        )
    }
}
