use std::borrow::Cow;
use std::fmt;

use serde::{Serialize, Deserialize};
use tracing::info;

use crate::state::State;

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct LiftoffPayload {
    #[serde(rename = "timestamp")]
    pub timestamp: String,
    pub latitude: Option<f32>,
    pub longitude: Option<f32>,
    /// will be false when player dismisses their ship from their SRV
    pub player_controlled: bool,
    #[serde(rename = "NearestDestination_Localised")]
    pub nearest_destination: Option<String>,
}

impl LiftoffPayload {
    pub fn process(self, state: &mut State) {
        state.docking_station = None;
    }

    pub fn log(&self) {
        let latitude =
            self.latitude
            .map_or(Cow::from("<not specified>"), |value| Cow::from(format!("{:.2}", value)));
        let longitude =
            self.longitude
            .map_or(Cow::from("<not specified>"), |value| Cow::from(format!("{:.2}", value)));
        let nearest_destination =
            self.nearest_destination
            .as_ref()
            .map_or(Cow::from("<not specified>"), |value| Cow::from(value));

        info!(
            %self.timestamp,
            %latitude,
            %longitude,
            self.player_controlled,
            %nearest_destination,
            "{}",
            self,
        );
    }
}

impl fmt::Display for LiftoffPayload {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        if let (Some(latitude), Some(longitude)) = (self.latitude, self.longitude) {
            if let Some(nearest_destination) = &self.nearest_destination {
                write!(f, "Liftoff from {:.2}, {:.2} near {}", latitude, longitude, nearest_destination)
            } else {
                write!(f, "Liftoff from {:.2}, {:.2}", latitude, longitude)
            }
        } else {
            write!(f, "Liftoff")
        }
    }
}
