use std::fmt;

use serde::{Serialize, Deserialize};
use tracing::info;

use crate::state::State;

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct LeaveBodyPayload {
    #[serde(rename = "timestamp")]
    pub timestamp: String,
    /// star system name
    pub star_system: String,
    pub system_address: u64,
    pub body: String,
    /// body number in system
    #[serde(rename = "BodyID")]
    pub body_id: u32,
}

impl LeaveBodyPayload {
    pub fn process(&self, state: &mut State) {
        state.nearby_body_name = None;
    }

    pub fn log(&self) {
        info!(
            %self.timestamp,
            %self.star_system,
            self.system_address,
            %self.body,
            self.body_id,
            "{}",
            self,
        );
    }
}

impl fmt::Display for LeaveBodyPayload {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "Goodbye {}", self.body)
    }
}
