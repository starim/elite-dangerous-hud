use std::fmt;

use serde::{Serialize, Deserialize};
use tracing::info;

use crate::state::State;

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct SupercruiseExitPayload {
    #[serde(rename = "timestamp")]
    pub timestamp: String,
    /// star system name
    pub star_system: String,
    pub body: String,
    #[serde(rename = "BodyID")]
    pub body_id: u32,
    pub body_type: String,
}

impl SupercruiseExitPayload {
    pub fn process(self, _state: &mut State) {}

    pub fn log(&self) {
        info!(
            %self.timestamp,
            %self.star_system,
            %self.body,
            self.body_id,
            %self.body_type,
            "{}",
            self,
        );
    }
}

impl fmt::Display for SupercruiseExitPayload {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "Arrived at {} {} in {}",
            self.body_type,
            self.body,
            self.star_system,
        )
    }
}
