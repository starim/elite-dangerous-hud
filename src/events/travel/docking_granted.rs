use std::fmt;

use serde::{Serialize, Deserialize};
use tracing::info;

use crate::state::{DockingRequest, State};

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct DockingGrantedPayload {
    #[serde(rename = "timestamp")]
    pub timestamp: String,
    pub station_name: String,
    pub station_type: String,
    #[serde(rename = "MarketID")]
    pub market_id: u64,
    pub landing_pad: u32,
}

impl DockingGrantedPayload {
    pub fn process(self, state: &mut State) {
        state.active_docking_request = Some(DockingRequest::from(self));
    }

    pub fn log(&self) {
        info!(
            %self.timestamp,
            %self.station_name,
            %self.station_type,
            self.market_id,
            self.landing_pad,
            "{}",
            self,
        );
    }
}

impl fmt::Display for DockingGrantedPayload {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "Granted docking permission for {} landing pad {}",
            self.station_name,
            self.landing_pad,
        )
    }
}
