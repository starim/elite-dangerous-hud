use std::borrow::Cow;
use std::convert::TryFrom;
use std::fmt;

use serde::{Serialize, Deserialize};
use tracing::info;

use crate::state::{PlanetaryTouchDownInfo, State};

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct TouchdownPayload {
    #[serde(rename = "timestamp")]
    pub timestamp: String,
    /// star system name
    pub star_system: Option<String>,
    pub system_address: Option<u64>,
    pub body: Option<String>,
    #[serde(rename = "BodyID")]
    pub body_id: Option<u32>,
    pub on_station: Option<bool>,
    pub on_planet: Option<bool>,
    /// will be false if the player's ship touches down while being recalled from an SRV
    pub player_controlled: bool,

    /// only if there's a point of interest within 50km
    #[serde(rename = "NearestDestination_Localised")]
    pub nearest_destination: Option<String>,

    /// only if the player is piloting
    pub latitude: Option<f32>,
    pub longitude: Option<f32>,
}

impl TouchdownPayload {
    pub fn process(self, state: &mut State) {
        if let Ok(planetary_touch_down_info) = PlanetaryTouchDownInfo::try_from(self) {
            state.planetary_touch_down = Some(planetary_touch_down_info);
        }
    }

    pub fn log(&self) {
        let star_system =
            self.star_system
            .as_ref()
            .map_or("<not specified>", |value| &*value);
        let system_address =
            self.system_address
            .as_ref()
            .map_or(Cow::from("<not specified>"), |value| Cow::from(value.to_string()));
        let body =
            self.body
            .as_ref()
            .map_or("<not specified>", |value| &*value);
        let body_id =
            self.body_id
            .as_ref()
            .map_or(Cow::from("<not specified>"), |value| Cow::from(value.to_string()));
        let on_station =
            self.on_station
            .as_ref()
            .map_or(Cow::from("<not specified>"), |value| Cow::from(value.to_string()));
        let on_planet =
            self.on_planet
            .as_ref()
            .map_or(Cow::from("<not specified>"), |value| Cow::from(value.to_string()));
        let latitude =
            self.latitude
            .as_ref()
            .map_or(Cow::from("<not specified>"), |value| Cow::from(format!("{:.2}", value)));
        let longitude =
            self.longitude
            .as_ref()
            .map_or(Cow::from("<not specified>"), |value| Cow::from(format!("{:.2}", value)));

        let nearest_destination =
            self.nearest_destination
            .as_ref()
            .map_or("None", |value| &*value);

        info!(
            %self.timestamp,
            star_system,
            %system_address,
            body,
            %body_id,
            %on_station,
            %on_planet,
            self.player_controlled,
            %nearest_destination,
            %latitude,
            %longitude,
            "{}",
            self,
        );
    }
}

impl fmt::Display for TouchdownPayload {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let body_text = match self.body {
            Some(ref body) => &*body,
            None => "unknown body",
        };

        if let (
            Some(nearest_destination),
            Some(latitude),
            Some(longitude),
        ) = (
            &self.nearest_destination,
            &self.latitude,
            &self.longitude,
        ) {
            write!(
                f,
                "Touched down at {:.2}, {:.2} on {} near {}",
                latitude,
                longitude,
                body_text,
                nearest_destination,
            )
        } else if let (Some(latitude), Some(longitude)) = (self.latitude, self.longitude) {
            write!(
                f,
                "Touched down at {:.2}, {:.2} on {}",
                latitude,
                longitude,
                body_text,
            )
        } else {
            if let Some(nearest_destination) = &self.nearest_destination {
                write!(
                    f,
                    "Touched down on {} near {}",
                    body_text,
                    nearest_destination,
                )
            } else {
                write!(f, "Touched down on {}", body_text)
            }
        }
    }
}
