use std::borrow::Cow;
use std::fmt;

use serde::{Serialize, Deserialize};
use tracing::info;

use crate::state::State;

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct StartJumpPayload {
    #[serde(rename = "timestamp")]
    pub timestamp: String,
    /// Hyperspace or Supercruise
    pub jump_type: String,
    
    // if a hyperspace jump
    pub star_system: Option<String>,
    pub system_address: Option<u64>,
    /// K, G, B, F, etc.
    pub star_class: Option<String>,
}

impl StartJumpPayload {
    pub fn process(self, state: &mut State) {
        if let Some(ref mut nav_route) = state.nav_route {
            if self.jump_type == "Hyperspace" {
                nav_route.route.pop_front();
            }
        }
    }

    pub fn log(&self) {
        let star_system =
            self.star_system
            .as_ref()
            .map_or("<not specified>", |value| &*value);
        let system_address =
            self.system_address
            .map_or(Cow::from("<not specified>"), |value| Cow::from(value.to_string()));
        let star_class =
            self.star_class
            .as_ref()
            .map_or("<not specified>", |value| &*value);

        info!(
            %self.timestamp,
            %self.jump_type,
            %star_system,
            %system_address,
            %star_class,
            "{}",
            self,
        );
    }
}

impl fmt::Display for StartJumpPayload {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let jump_type = self.jump_type.to_ascii_lowercase();
        if let Some(ref star_system) = self.star_system {
            write!(f, "Started {} jump to {}", jump_type, star_system)
        } else {
            write!(f, "Started {} jump", jump_type)
        }
    }
}
