use std::borrow::Cow;
use std::convert::TryFrom;
use std::fmt;

use serde::{Serialize, Deserialize};
use tracing::info;

use crate::state::{Conflict, DockingStationInfo, Economy, Faction, State, SystemInfo};

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct LocationPayload {
    #[serde(rename = "timestamp")]
    pub timestamp: String,
    pub dist_from_star_ls: Option<f32>,
    pub docked: bool,
    pub station_name: Option<String>,
    /// Outpost, Crater Outpost, etc.
    pub station_type: Option<String>,
    #[serde(rename = "MarketID")]
    pub market_id: Option<u64>,
    /// star system name
    pub star_system: String,
    pub system_address: u64,
    pub star_pos: Vec<f32>,
    /// Federation, Empire, Alliance, etc.
    pub system_allegiance: String,
    pub system_faction: Option<Faction>,
    pub system_economy: String,
    #[serde(rename = "SystemSecondEconomy_Localised")]
    pub system_second_economy: String,
    #[serde(rename = "SystemGovernment_Localised")]
    pub system_government: String,
    /// Anarchy, Medium Security, etc.
    #[serde(rename = "SystemSecurity_Localised")]
    pub system_security: String,
    pub population: u64,
    /// if not close to a body the game will report the body as the system name plus the
    /// designations of all the stars in the system (e.g. "Synuefe DC-S d5-11 ABCD" for a system
    /// with four stars)
    pub body: String,
    #[serde(rename = "BodyID")]
    pub body_id: u32,
    /// Planet, etc.
    /// If not close to a body, the game will report a body type of "Null".
    pub body_type: String,
    pub factions: Option<Vec<Faction>>,
    pub conflicts: Option<Vec<Conflict>>,

    // will only be populated if docked in a station
    pub station_faction: Option<Faction>,
    #[serde(rename = "StationGovernment_Localised")]
    pub station_government: Option<String>,
    pub station_allegiance: Option<String>,
    pub station_services: Option<Vec<String>>,
    #[serde(rename = "StationEconomy_Localised")]
    pub station_economy: Option<String>,
    pub station_economies: Option<Vec<Economy>>,

    // Odyssey:
    pub taxi: Option<bool>,
    pub multicrew: Option<bool>,
    pub in_srv: Option<bool>,
    pub on_foot: Option<bool>,
    /// only Odyssey?
    pub wanted: Option<String>,
}

impl LocationPayload {
    pub fn process(self, state: &mut State) {
        if let Ok(station_info) = DockingStationInfo::try_from(self.clone()) {
            state.docking_station = Some(station_info);
        }
        state.current_system = SystemInfo::from(self);
    }

    pub fn log(&self) {
        let dist_from_star_ls =
            self.dist_from_star_ls
            .as_ref()
            .map_or(Cow::from("<not specified>"), |value| Cow::from(format!("{:.2} Ls", value)));

        let star_pos_vec: Vec<String> =
            self.star_pos
            .iter()
            .map(|pos| format!("{:.2}", pos))
            .collect();
        let star_pos = format!("[{}]", star_pos_vec.join(", "));

        let factions = match &self.factions {
            Some(faction_list) => {
                Cow::from(
                    faction_list.iter()
                    .map(|faction| &*faction.name)
                    .collect::<Vec<&str>>()
                    .join(", ")
                )
            },
            None => Cow::from("None"),
        };

        let conflicts = match &self.conflicts {
            Some(conflict_list) => {
                Cow::from(
                    conflict_list.iter()
                    .map(|conflict| conflict.to_string())
                    .collect::<Vec<String>>()
                    .join(", ")
                )
            },
            None => Cow::from("None"),
        };

        let market_id =
            self.station_name
            .as_ref()
            .map_or(Cow::from("None"), |value| Cow::from(format!("{}", value)));

        let station_name =
            self.station_name
            .as_ref()
            .map_or("None", |value| &*value);

        let station_type =
            self.station_type
            .as_ref()
            .map_or("None", |value| &*value);

        let station_faction =
            self.station_faction
            .as_ref()
            .map_or("None", |faction| &*faction.name);

        let station_government =
            self.station_government
            .as_ref()
            .map_or("None", |value| &*value);

        let station_allegiance =
            self.station_allegiance
            .as_ref()
            .map_or("None", |value| &*value);

        let station_services =
            self.station_services
            .as_ref()
            .map_or(Cow::from("None"), |value| Cow::from(value.join(", ")));

        let station_economy =
            self.station_economy
            .as_ref()
            .map_or("None", |value| &*value);

        let station_economies =
            self.station_economies
            .as_ref()
            .map_or(Cow::from("None"), |economies| {
                Cow::from(
                    economies
                    .iter()
                    .map(|economy| economy.to_string())
                    .collect::<Vec<String>>()
                    .join(", ")
                )
            });

        let taxi = self.taxi.map_or(Cow::from("<not specified>"), |value| Cow::from(value.to_string()));

        let multicrew = self.multicrew.map_or(Cow::from("<not specified>"), |value| Cow::from(value.to_string()));

        let in_srv = self.in_srv.map_or(Cow::from("<not specified>"), |value| Cow::from(value.to_string()));

        let on_foot = self.on_foot.map_or(Cow::from("<not specified>"), |value| Cow::from(value.to_string()));

        let body_type = self.body_type().map_or(
            "None",
            |value| value,
        );

        info!(
            %self.timestamp,
            %dist_from_star_ls,
            self.docked,
            %station_name,
            %station_type,
            %market_id,
            %self.star_system,
            self.system_address,
            %star_pos,
            %self.system_allegiance,
            %self.system_economy,
            %self.system_second_economy,
            %self.system_government,
            %self.system_security,
            self.population,
            %self.body,
            self.body_id,
            %body_type,
            %factions,
            %conflicts,
            station_faction,
            station_government,
            station_allegiance,
            %station_services,
            station_economy,
            %station_economies,
            %taxi,
            %multicrew,
            %in_srv,
            %on_foot,
            "{}",
            self,
        );
    }

    pub fn body_type(&self) -> Option<&str> {
        match &*self.body_type {
            "Null" => None,
            body_type => Some(body_type),
        }
    }
}

impl fmt::Display for LocationPayload {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let owning_power_adjective: Cow<str> = match &*self.system_allegiance {
            "Empire" => "imperial".into(),
            "Federation" => "Federation".into(),
            "Alliance" => "Alliance".into(),
            _ => self.system_allegiance.to_ascii_lowercase().into(),
        };

        let station_description = match &self.station_name {
            Some(station_name) => Cow::from(format!(
                "{} {} {} at",
                owning_power_adjective,
                self.station_type.as_ref().unwrap().to_ascii_lowercase(),
                station_name,
            )),
            None => Cow::from("open space in"),
        };

        let body_description = match self.body_type() {
            Some(body_type) => {
                format!(
                    "{} {} in {}",
                    body_type.to_ascii_lowercase(),
                    self.body,
                    self.star_system,
                )
            },
            None => format!("system {}", self.star_system),
        };

        write!(
            f,
            "New location is {} {}",
            station_description,
            body_description,
        )
    }
}
