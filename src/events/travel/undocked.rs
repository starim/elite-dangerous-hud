use std::fmt;

use serde::{Serialize, Deserialize};
use tracing::info;

use crate::state::State;

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct UndockedPayload {
    #[serde(rename = "timestamp")]
    pub timestamp: String,
    pub station_name: String,
    #[serde(rename = "MarketID")]
    pub market_id: u64,
    /// Outpost, Coriolis, etc.
    pub station_type: String,
}

impl UndockedPayload {
    pub fn process(self, state: &mut State) {
        state.docking_station = None;
    }

    pub fn log(&self) {
        info!(
            %self.timestamp,
            %self.station_name,
            self.market_id,
            %self.station_type,
            "{}",
            self,
        );
    }
}

impl fmt::Display for UndockedPayload {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "Undocked from {}", self.station_name)
    }
}
