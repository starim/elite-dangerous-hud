use std::borrow::Cow;
use std::fmt;

use serde::{Serialize, Deserialize};
use tracing::info;

use crate::state::{Conflict, Faction, State, SystemInfo};

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct FSDJumpPayload {
    #[serde(rename = "timestamp")]
    pub timestamp: String,
    pub star_system: String,
    pub system_address: u64,
    /// three-dimensional coordinates
    pub star_pos: Vec<f32>,
    /// star name
    pub body: String,
    pub jump_dist: f32,
    pub fuel_used: f32,
    pub fuel_level: f32,
    /// whether an FSD boost was used, and if so what type
    /// type codes aren't documented, but here's what I've found from observation:
    /// 1: basic FSD injection
    /// 2: standard FSD injection
    /// 3: premium FSD injection
    /// 4: boosted from a star (white dwarf or neutron star)
    pub boost_used: Option<i32>,
    pub system_faction: Option<Faction>,
    /// Federation, Empire, Alliance, etc.
    pub system_allegiance: String,
    #[serde(rename = "SystemEconomy_Localised")]
    pub system_economy: String,
    #[serde(rename = "SystemSecondEconomy_Localised")]
    pub system_second_economy: String,
    #[serde(rename = "SystemGovernment_Localised")]
    pub system_government: String,
    /// Anarchy, Medium Security, etc.
    #[serde(rename = "SystemSecurity_Localised")]
    pub system_security: String,
    pub factions: Option<Vec<Faction>>,
    pub population: u64,
    pub conflicts: Option<Vec<Conflict>>,

    /// only Odyssey?
    pub wanted: Option<String>,
}

impl FSDJumpPayload {
    pub fn process(self, state: &mut State) {
        state.current_system = SystemInfo::from(self);
    }

    pub fn log(&self) {
        let stringified_star_pos: Vec<String> =
            self.star_pos.iter()
            .map(|coord| format!("{:.2}", coord))
            .collect();
        let boost_used =
            self.boost_used
            .map_or(Cow::from("none"), |value| Cow::from(format!("boost type {}", value)));

        info!(
            %self.timestamp,
            %self.star_system,
            self.system_address,
            star_pos = %format!("({})", stringified_star_pos.join(", ")),
            %self.body,
            jump_dist = %format!("{:.2}", self.jump_dist),
            fuel_used = %format!("{:.2}", self.fuel_used),
            fuel_level = %format!("{:.2}", self.fuel_level),
            %boost_used,
            "{}",
            self,
        );
    }
}

impl fmt::Display for FSDJumpPayload {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "{:.2} ly jump to {} used {:.2} fuel (fuel level {:.2})",
            self.jump_dist,
            self.star_system,
            self.fuel_used,
            self.fuel_level,
        )
    }
}
