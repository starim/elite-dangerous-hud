use std::fmt;

use serde::{Serialize, Deserialize};
use tracing::info;

use crate::state::State;

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct ApproachBodyPayload {
    #[serde(rename = "timestamp")]
    pub timestamp: String,
    pub star_system: String,
    pub body: String,
}

impl ApproachBodyPayload {
    pub fn process(self, state: &mut State) {
        state.nearby_body_name = Some(self.body);
    }

    pub fn log(&self) {
        info!(
            %self.timestamp,
            %self.star_system,
            %self.body,
            "{}",
            self,
        );
    }
}

impl fmt::Display for ApproachBodyPayload {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "Approaching {} in {}", self.body, self.star_system)
    }
}
