use std::fmt;

use serde::{Serialize, Deserialize};
use tracing::info;

use crate::state::State;

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct DockingCancelledPayload {
    #[serde(rename = "timestamp")]
    pub timestamp: String,
    pub station_name: String,
    #[serde(rename = "MarketID")]
    pub market_id: u64,
}

impl DockingCancelledPayload {
    pub fn process(self, state: &mut State) {
        state.active_docking_request = None;
    }

    pub fn log(&self) {
        info!(
            %self.timestamp,
            %self.station_name,
            self.market_id,
            "{}",
            self,
        );
    }
}

impl fmt::Display for DockingCancelledPayload {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "Cancelled docking procedure for {}", self.station_name)
    }
}
