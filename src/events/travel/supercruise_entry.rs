use std::fmt;

use serde::{Serialize, Deserialize};
use tracing::info;

use crate::state::State;

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct SupercruiseEntryPayload {
    #[serde(rename = "timestamp")]
    pub timestamp: String,
    /// star system name
    pub star_system: String,
}

impl SupercruiseEntryPayload {
    pub fn process(self, _state: &mut State) {}

    pub fn log(&self) {
        info!(
            %self.timestamp,
            %self.star_system,
            "{}",
            self,
        );
    }
}

impl fmt::Display for SupercruiseEntryPayload {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "Entered supercruise")
    }
}
