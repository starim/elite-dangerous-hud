use std::fmt;

use serde::{Serialize, Deserialize};
use tracing::{info, error};

use crate::state::{NavRoute, State};

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct NavRoutePayload {
    #[serde(rename = "timestamp")]
    pub timestamp: String,
}

impl NavRoutePayload {
    pub fn process(self, state: &mut State) {
        match NavRoute::read() {
            Ok(nav_route) => state.nav_route = Some(nav_route),
            Err(error) => error!(%error),
        }
    }

    pub fn log(&self) {
        info!(
            %self.timestamp,
            "{}",
            self,
        );
    }
}

impl fmt::Display for NavRoutePayload {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let nav_route = match NavRoute::read() {
            Ok(nav_route) => nav_route,
            Err(error) => {
                error!(%error);
                return Ok(());
            }
        };

        fn is_scoopable(star_class: &str) -> bool {
            "KGBFOAM".contains(star_class)
        }

        let star_scoop_list = nav_route.route.iter().skip(1).map(|segment| {
           is_scoopable(&*segment.star_class)
        }).map(|scoopable| {
            if scoopable { "*" } else { "." }
        }).collect::<Vec<&str>>().join("");

        let star_list = nav_route.route.iter().skip(1).map(|segment| {
            let (star_description, percent_of_stars) = match &*segment.star_class {
                "O" => ("Main Sequence", 0.15),
                "A" => ("Main Sequence", 4.5),
                "B" => ("Main Sequence", 1.25),
                "F" => ("Main Sequence", 9.0),
                "G" => ("Main Sequence", 6.75),
                "K" => ("Main Sequence", 18.75),
                "M" => ("Main Sequence", 34.5),

                // TODO: sadly the game uses the same star class for supergiants and
                // regular stars so a more sophisticated algorithm is needed to determine
                // if this is an interesting supergiant star. The HUD display in game does
                // use the word supergiant to describe such a star, so it may be possible
                // to parse this information from a later ReceiveText message
                // "B" => ("Supergiant", 0.00175),
                // "A" => ("Supergiant", 0.023),
                // "F" => ("Supergiant", 0.00525),
                // "G" => ("Supergiant", 0.004),
                // "M" => ("Supergiant", 0.0035),
                // "K" => ("Giant", 0.05),
                // "M" => ("Giant", 0.16),

                "AeBe" => ("Herbig Ae/Be Proto Star", 0.096),
                "TTS" => ("Proto Star", 2.304),

                "C" => ("Carbon Star", 2.304),
                "CJ" => ("Carbon Star", 0.0024),
                "CN" => ("Carbon Star", 0.0192),
                "MS" => ("Carbon Star", 0.0288),
                "S" => ("Carbon Star", 0.0296),
                // these I couldn't find statistics for and just used the rarity of their class in
                // general divided by 10
                "CH" | "CHd" | "CS" => ("Carbon Star", 0.008),

                "W" => ("Wolf-Rayet Star", 0.00022),
                "WC" => ("Wolf-Rayet Star", 0.0105),
                "WN" => ("Wolf-Rayet Star", 0.0075),
                "WNC" => ("Wolf-Rayet Star", 0.008),
                "WO" => ("Wolf-Rayet Star", 0.235),

                // TODO: "black hole" probably isn't the right star class code
                "Black Hole" | "Supermassive Black Hole" => ("Black Hole", 0.41),

                "N" => ("Neutron Star", 4.0),

                "D" => ("White Dwarf", 0.00324),
                "DA"  => ("White Dwarf", 0.1044),
                "DAB"  => ("White Dwarf", 0.0432),
                "DAV"  => ("White Dwarf", 0.01152),
                "DAZ"  => ("White Dwarf", 0.001548),
                "DB"  => ("White Dwarf", 0.01836),
                "DBV"  => ("White Dwarf", 0.0036),
                "DBZ"  => ("White Dwarf", 0.000396),
                "DC"  => ("White Dwarf", 0.1584),
                "DCV"  => ("White Dwarf", 0.01368),
                "DQ"  => ("White Dwarf", 0.000072),
                // these I couldn't find statistics for and just used the rarity of their class in
                // general
                "DAO" | "DO" | "DOV"  => ("White Dwarf", 0.000324),

                "L" => ("Brown Dwarf", 8.7),
                "T" => ("Brown Dwarf", 4.05),
                "Y" => ("Brown Dwarf", 2.25),

                _ => ("Unrecognized Star Type", f32::NAN)
            };

            let scoopable_indicator = if is_scoopable(&*segment.star_class) {
                "*"
            } else {
                "x"
            };

            let rarity = match percent_of_stars {
                // sentinal value for unrecognized star type
                p if p.is_nan() => "?",

                p if p > 10.0 => "Common",
                p if p >  1.0 => "Uncommon",
                p if p >  0.1 => "Rare",
                _             => "Very Rare",
            };

            format!(
                "\t{} {}\n\t\t{} {} ({})",
                scoopable_indicator,
                segment.star_system,
                segment.star_class,
                star_description,
                rarity,
            )
        }).collect::<Vec<String>>().join("\n");

        write!(
            f,
            "New hyperspace route plotted.\nScoopable stars in route: {}\nRoute:\n{}\n",
            star_scoop_list,
            star_list,
        )
    }
}
