use std::fmt;

use serde::{Serialize, Deserialize};
use tracing::info;

use crate::state::State;

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct DockingDeniedPayload {
    #[serde(rename = "timestamp")]
    pub timestamp: String,
    pub station_name: String,
    pub station_type: String,
    #[serde(rename = "MarketID")]
    pub market_id: u64,
    /// reason can be one of: NoSpace, TooLarge, Hostile, Offences, Distance, ActiveFighter,
    /// NoReason
    pub reason: String,
}

impl DockingDeniedPayload {
    pub fn process(self, _state: &mut State) {}

    pub fn log(&self) {
        info!(
            %self.timestamp,
            %self.station_name,
            %self.station_type,
            self.market_id,
            "{}",
            self,
        );
    }
}

impl fmt::Display for DockingDeniedPayload {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{} denied docking request: {}", self.station_name, self.reason)
    }
}
