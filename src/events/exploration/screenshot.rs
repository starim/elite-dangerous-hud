use std::borrow::Cow;
use std::fmt;
use std::fs;
use std::path::PathBuf;

use chrono::{DateTime, Utc, Local};
use image::io::Reader as ImageReader;
use serde::{Serialize, Deserialize};
use tracing::{info, warn, error};

use crate::CONFIG;
use crate::error::Error;
use crate::state::State;

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct ScreenshotPayload {
    #[serde(rename = "timestamp")]
    pub timestamp: DateTime<Utc>,
    pub filename: String,
    pub width: i32,
    pub height: i32,
    pub system: Option<String>,
    pub body: Option<String>,

    // only if on a planet or in low-altitude flight
    pub latitude: Option<f32>,
    pub longitude: Option<f32>,
    pub altitude: Option<f32>,
    pub heading: Option<i32>,
}

impl ScreenshotPayload {
    pub fn process(&self, state: &mut State) {
        match self.tag_screenshot() {
            Ok(new_path) => {
                info!(
                    screenshot_path = %new_path.to_string_lossy(),
                    "Saved new screenshot to {}",
                    new_path.to_string_lossy(),
                );
                state.log_message(
                    format!("Saved new screenshot to {}", new_path.to_string_lossy())
                );
            },
            Err(error) => {
                error!(?error, "Failed to tag screenshot");
                state.log_message(
                    format!("ERROR: Failed to tag screenshot: {}", error)
                );
            },
        }
    }

    pub fn log(&self) {
        let system = self.system.as_ref().map_or("<not specified>", |value| &*value);

        let body = self.body.as_ref().map_or("<not specified>", |value| &*value);

        let latitude =
            self.latitude
            .map_or(Cow::from("<not specified>"), |value| Cow::from(format!("{:.2}", value)));

        let longitude =
            self.longitude
            .map_or(Cow::from("<not specified>"), |value| Cow::from(format!("{:.2}", value)));

        let altitude =
            self.altitude
            .map_or(Cow::from("<not specified>"), |value| Cow::from(format!("{:.2} km", value/1_000.0)));

        let heading =
            self.heading
            .map_or(Cow::from("<not specified>"), |value| Cow::from(format!("{}°", value)));

        info!(
            %self.timestamp,
            %self.filename,
            %self.width,
            %self.height,
            system,
            body,
            %latitude,
            %longitude,
            %altitude,
            %heading,
            "{}",
            self,
        );
    }

    /// the filename reported by the game includes an "\ED_Pictures\" prefix; this function gives
    /// the actual filename of the screenshot
    pub fn true_filename(&self) -> &str {
        match self.filename.rsplit_once("\\") {
            Some(parts) => parts.1,
            None => &*self.filename,
        }
    }

    fn new_name_stem(&self) -> String {
        let date =
            self.timestamp
            .with_timezone(&Local)
            .format("%F");

        if let (Some(system), Some(body)) = (&self.system, &self.body) {
            // avoid redundant screenshot names like
            // "HIP 38129, HIP 38129 B 4 (2021-06-15) 01.jpg"
            if body.starts_with(&*system) {
                format!("{} ({})", body, date)
            } else {
                format!("{}, {} ({})", system, body, date)
            }
        } else {
            format!("main menu ({})", date)
        }
    }

    pub fn new_filename(&self) -> String {
        let screenshots_dir = unsafe {
            CONFIG.as_ref().unwrap().screenshots_target_dir()
        };
        let screenshot_name_stem = self.new_name_stem();
        let existing_screenshots_glob = format!(
            "{}/{}*",
            screenshots_dir.to_string_lossy(),
            screenshot_name_stem,
        );

        let existing_screenshot_count =
            glob::glob(&*existing_screenshots_glob)
            .expect("Unable to parse screenshots glob")
            .count();

        let file_extension = unsafe {
            CONFIG.as_ref().unwrap().screenshot_target_format()
        };
        let next_number = existing_screenshot_count + 1;
        format!("{} {:02}.{}", screenshot_name_stem, next_number, file_extension)
    }

    pub fn tag_screenshot(&self) -> Result<PathBuf, Error> {
        let original_path = {
            let mut path = crate::files::game_screenshots_dir_path();
            path.push(self.true_filename());
            path
        };
        let image =
            ImageReader::open(&original_path)
            .map_err(|error| {
                let context = "Failed opening original screenshot file";
                Error::new(context.into(), Box::new(error))
            })?
            .decode().map_err(|error| {
                let context = "Failed reading original screenshot contents as image";
                Error::new(context.into(), Box::new(error))
            })?;

        let new_path = {
            let mut path = crate::files::screenshots_target_dir_path();
            path.push(self.new_filename());
            path
        };

        image.save(&new_path).map_err(|error| {
            let context = "Failed saving screenshot to new location";
            Error::new(context.into(), Box::new(error))
        })?;

        if let Err(error) = fs::remove_file(&original_path) {
            warn!(
                ?error,
                path = %original_path.to_string_lossy(),
                "Error deleting original screenshot after tagging",
            );
        }

        Ok(new_path)
    }
}

impl fmt::Display for ScreenshotPayload {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        if let (Some(system), Some(body)) = (&self.system, &self.body) {
            write!(f, "Screenshot {} taken near {} in {}", self.true_filename(), system, body)
        } else {
            write!(f, "Screenshot {} taken of main menu", self.true_filename())
        }
    }
}
