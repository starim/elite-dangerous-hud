use std::fmt;

use serde::{Serialize, Deserialize};
use tracing::info_span;

use crate::state::State;

// event types
pub mod exploration;
pub mod travel;
pub mod other;
pub mod startup;

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "PascalCase")]
#[serde(tag = "event")]
pub enum Event {
    // travel
    ApproachBody(travel::ApproachBodyPayload),
    Docked(travel::DockedPayload),
    DockingCancelled(travel::DockingCancelledPayload),
    DockingDenied(travel::DockingDeniedPayload),
    DockingGranted(travel::DockingGrantedPayload),
    DockingRequested(travel::DockingRequestedPayload),
    DockingTimeout(travel::DockingTimeoutPayload),
    FSDJump(travel::FSDJumpPayload),
    FSDTarget(travel::FSDTargetPayload),
    LeaveBody(travel::LeaveBodyPayload),
    Liftoff(travel::LiftoffPayload),
    /// happens on startup or when the player is resurrected at a station
    Location(travel::LocationPayload),
    StartJump(travel::StartJumpPayload),
    SupercruiseEntry(travel::SupercruiseEntryPayload),
    SupercruiseExit(travel::SupercruiseExitPayload),
    Touchdown(travel::TouchdownPayload),
    Undocked(travel::UndockedPayload),
    /// happens when the NavRoute.json file is updated
    NavRoute(travel::NavRoutePayload),

    // exploration
    Screenshot(exploration::ScreenshotPayload),

    // other
    Shutdown(other::ShutdownPayload),

    // startup
    LoadGame(startup::LoadGamePayload),
    Loadout(startup::LoadoutPayload),
}

impl Event {
    pub fn new(json: &str) -> serde_json::Result<Self> {
        serde_json::from_str(json)
    }

    pub fn name(&self) -> &str {
        match self {
            Event::ApproachBody(_) => "ApproachBody",
            Event::Docked(_) => "Docked",
            Event::DockingCancelled(_) => "DockingCancelled",
            Event::DockingDenied(_) => "DockingDenied",
            Event::DockingGranted(_) => "DockingGranted",
            Event::DockingRequested(_) => "DockingRequested",
            Event::DockingTimeout(_) => "DockingTimeout",
            Event::FSDJump(_) => "FSDJump",
            Event::FSDTarget(_) => "FSDTarget",
            Event::LeaveBody(_) => "LeaveBody",
            Event::Liftoff(_) => "Liftoff",
            Event::LoadGame(_) => "LoadGame",
            Event::Loadout(_) => "Loadout",
            Event::Location(_) => "Location",
            Event::StartJump(_) => "StartJump",
            Event::SupercruiseEntry(_) => "SupercruiseEntry",
            Event::SupercruiseExit(_) => "SupercruiseExit",
            Event::Touchdown(_) => "Touchdown",
            Event::Undocked(_) => "Undocked",
            Event::NavRoute(_) => "NavRoute",
            Event::Screenshot(_) => "Screenshot",
            Event::Shutdown(_) => "Shutdown",
        }
    }

    pub fn log(&self) {
        match self {
            Event::ApproachBody(payload) => payload.log(),
            Event::Docked(payload) => payload.log(),
            Event::DockingCancelled(payload) => payload.log(),
            Event::DockingDenied(payload) => payload.log(),
            Event::DockingGranted(payload) => payload.log(),
            Event::DockingRequested(payload) => payload.log(),
            Event::DockingTimeout(payload) => payload.log(),
            Event::FSDJump(payload) => payload.log(),
            Event::FSDTarget(payload) => payload.log(),
            Event::LeaveBody(payload) => payload.log(),
            Event::Liftoff(payload) => payload.log(),
            Event::LoadGame(payload) => payload.log(),
            Event::Loadout(payload) => payload.log(),
            Event::Location(payload) => payload.log(),
            Event::StartJump(payload) => payload.log(),
            Event::SupercruiseEntry(payload) => payload.log(),
            Event::SupercruiseExit(payload) => payload.log(),
            Event::Touchdown(payload) => payload.log(),
            Event::Undocked(payload) => payload.log(),
            Event::NavRoute(payload) => payload.log(),
            Event::Screenshot(payload) => payload.log(),
            Event::Shutdown(payload) => payload.log(),
        }
    }

    pub fn process(self, state: &mut State) {
        info_span!("Processing Event", event = self.name());
        // always log the event
        self.log();
        state.log_message(self.to_string());

        match self {
            Event::ApproachBody(payload) => payload.process(state),
            Event::Docked(payload) => payload.process(state),
            Event::DockingCancelled(payload) => payload.process(state),
            Event::DockingDenied(payload) => payload.process(state),
            Event::DockingGranted(payload) => payload.process(state),
            Event::DockingRequested(payload) => payload.process(state),
            Event::DockingTimeout(payload) => payload.process(state),
            Event::FSDJump(payload) => payload.process(state),
            Event::FSDTarget(payload) => payload.process(state),
            Event::LeaveBody(payload) => payload.process(state),
            Event::Liftoff(payload) => payload.process(state),
            Event::LoadGame(payload) => payload.process(state),
            Event::Loadout(payload) => payload.process(state),
            Event::Location(payload) => payload.process(state),
            Event::StartJump(payload) => payload.process(state),
            Event::SupercruiseEntry(payload) => payload.process(state),
            Event::SupercruiseExit(payload) => payload.process(state),
            Event::Touchdown(payload) => payload.process(state),
            Event::Undocked(payload) => payload.process(state),
            Event::NavRoute(payload) => payload.process(state),
            Event::Screenshot(payload) => payload.process(state),
            Event::Shutdown(payload) => payload.process(state),
        }
    }

    /// Returns the game data file that was updated if this event notifies of an update to a data
    /// file. If this event doesn't correspond to an update of a game data file, returns None.
    pub fn updated_file(&self) -> Option<&'static str> {
        match *self {
            Event::NavRoute(_) => Some("NavRoute.json"),
            _ => None,
        }
    }
}

impl fmt::Display for Event {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Event::ApproachBody(payload) => write!(f, "{}: {}", self.name(), payload.to_string()),
            Event::Docked(payload) => write!(f, "{}: {}", self.name(), payload.to_string()),
            Event::DockingCancelled(payload) => write!(f, "{}: {}", self.name(), payload.to_string()),
            Event::DockingDenied(payload) => write!(f, "{}: {}", self.name(), payload.to_string()),
            Event::DockingGranted(payload) => write!(f, "{}: {}", self.name(), payload.to_string()),
            Event::DockingRequested(payload) => write!(f, "{}: {}", self.name(), payload.to_string()),
            Event::DockingTimeout(payload) => write!(f, "{}: {}", self.name(), payload.to_string()),
            Event::FSDJump(payload) => write!(f, "{}: {}", self.name(), payload.to_string()),
            Event::FSDTarget(payload) => write!(f, "{}: {}", self.name(), payload.to_string()),
            Event::LeaveBody(payload) => write!(f, "{}: {}", self.name(), payload.to_string()),
            Event::Liftoff(payload) => write!(f, "{}: {}", self.name(), payload.to_string()),
            Event::LoadGame(payload) => write!(f, "{}: {}", self.name(), payload.to_string()),
            Event::Loadout(payload) => write!(f, "{}: {}", self.name(), payload.to_string()),
            Event::Location(payload) => write!(f, "{}: {}", self.name(), payload.to_string()),
            Event::StartJump(payload) => write!(f, "{}: {}", self.name(), payload.to_string()),
            Event::SupercruiseEntry(payload) => write!(f, "{}: {}", self.name(), payload.to_string()),
            Event::SupercruiseExit(payload) => write!(f, "{}: {}", self.name(), payload.to_string()),
            Event::Touchdown(payload) => write!(f, "{}: {}", self.name(), payload.to_string()),
            Event::Undocked(payload) => write!(f, "{}: {}", self.name(), payload.to_string()),
            Event::NavRoute(payload) => write!(f, "{}: {}", self.name(), payload.to_string()),
            Event::Screenshot(payload) => write!(f, "{}: {}", self.name(), payload.to_string()),
            Event::Shutdown(payload) => write!(f, "{}: {}", self.name(), payload.to_string()),
        }
    }
}
