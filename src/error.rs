use std::borrow::Cow;
use std::fmt::{self, Display, Formatter};

#[derive(Debug)]
pub struct Error {
    context: Cow<'static, str>,
    error: Box<dyn std::error::Error>
}

impl Error {
    pub fn new(message: Cow<'static, str>, error: Box<dyn std::error::Error>) -> Self {
        Error {
            context: message,
            error: error,
        }
    }
}

impl Display for Error {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(f,"{}: {:?}", self.context, self.error)
    }
}

impl std::error::Error for Error {
    fn description(&self) -> &str {
        &*self.context
    }
}
