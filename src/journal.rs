use notify::{DebouncedEvent, RecursiveMode, Watcher, watcher};
use tracing::{info, warn, error};

use std::fs::File;
use std::io::{self, BufRead, BufReader};
use std::path::PathBuf;
use std::sync::mpsc::{channel, Sender, Receiver};
use std::time::Duration;

use eframe::egui;
use notify::inotify::INotifyWatcher;
use serde_json::error::Category;

use crate::events::Event;

pub struct Journal {
    journal_path: PathBuf,
    watcher: INotifyWatcher,
    rx: Receiver<DebouncedEvent>,
    last_read_journal_line: usize,
    event_sender: Sender<Event>,
}

impl Journal {
    pub fn new(event_sender: Sender<Event>) -> Self {
        let journal_path = Journal::current_journal_path();
        info!(journal_file = &*journal_path.to_string_lossy(), "Found current journal");

        let (tx, rx) = channel();
        let watcher = watcher(tx, Duration::from_millis(500)).unwrap();

        Journal { journal_path, watcher, rx, last_read_journal_line: 0, event_sender }
    }


    pub fn current_journal_path() -> PathBuf {
        // journal filename format: "Journal.<datestamp>.<part>.log"
        let journal_file_glob = format!("{}/Journal.*.log", crate::files::game_data_dir_path().to_string_lossy());

        let mut journal_files: Vec<PathBuf> =
            glob::glob(&*journal_file_glob)
            .expect("Unable to parse journal glob")
            .filter_map(|maybe_entry| {
                if let Err(ref error) = maybe_entry {
                    warn!(?error, "Error scanning file for latest journal file");
                }
                maybe_entry.ok()
            })
            .collect();

        journal_files.sort_by_cached_key(|file| {
            let filename = file.file_name().unwrap().to_string_lossy();
            let time_segment =
                filename
                .strip_prefix("Journal.")
                .unwrap()
                .strip_suffix(".log")
                .unwrap()
                .to_string();

            time_segment
        });

        match journal_files.last() {
            Some(journal) => journal.to_path_buf(),
            None => panic!("FATAL: No journal files found"),
        }
    }

    pub fn process_initial_entries(&mut self, gui_context: Option<eframe::egui::Context>) {
        match self.next_journal_entries(gui_context) {
            Ok(new_last_read_journal_line) => self.last_read_journal_line = new_last_read_journal_line,
            Err(error) => error!(?error, "Error attempting to read initial events from journal"),
        }
    }

    /// This function will never return.
    pub fn permanent_watch_loop(&mut self, gui_context: egui::Context) {
        self.watcher.watch(self.journal_path.as_path(), RecursiveMode::NonRecursive).unwrap();

        loop {
            match self.rx.recv() {
                Ok(event) => {

                    match event {
                        DebouncedEvent::Remove(_) => self.rewatch(),
                        DebouncedEvent::Write(_) => {
                            match self.next_journal_entries(Some(gui_context.clone())) {
                                Ok(new_last_read_journal_line) => self.last_read_journal_line = new_last_read_journal_line,
                                Err(error) => error!(?error, "Error attempting to read new events from journal"),
                            }
                        },
                        DebouncedEvent::Error(error, _) => error!(?error, "Error watching journal"),
                        DebouncedEvent::NoticeWrite(_) => (),
                        DebouncedEvent::NoticeRemove(_) => (),
                        DebouncedEvent::Create(_) => (),
                        DebouncedEvent::Chmod(_) => (),
                        DebouncedEvent::Rename(_, _) => (),
                        DebouncedEvent::Rescan => (),
                    }
                },
                Err(error) => error!(?error, "Error watching journal"),
            }
        }
    }

    pub fn rewatch(&mut self) {
        if let Err(error) = self.watcher.unwatch(&self.journal_path) {
            match error {
                notify::Error::WatchNotFound => (),
                _ => warn!(?error, "Error unwatching journal after Remove event"),
            }
        }

        self.watcher.watch(&self.journal_path, RecursiveMode::NonRecursive).unwrap();
    }

    fn next_journal_entries(&self, gui_context: Option<eframe::egui::Context>) -> io::Result<usize> {
        let journal_file = File::open(&self.journal_path)?;
        let reader = BufReader::new(journal_file);

        let mut current_line_number = self.last_read_journal_line;
        for maybe_line in reader.lines().skip(self.last_read_journal_line) {
            current_line_number += 1;
            match maybe_line {
                Ok(line) => {
                    match Event::new(&*line) {
                        Ok(event) => {
                            match self.event_sender.send(event) {
                                Ok(()) => {
                                    if let Some(ref context) = gui_context {
                                        context.request_repaint();
                                    } else {
                                        warn!("Not requesting repaint because no context is available");
                                    }
                                },
                                Err(error) => error!(?error, "Journal failed to send event to the Gui thread because the receiver has disconnected."),
                            }
                        },
                        Err(error) => {
                            match error.classify() {
                                Category::Io => {
                                    error!(json = %line, ?error, "I/O error parsing journal event from journal file");
                                },
                                Category::Syntax => {
                                    error!(json = %line, ?error, "Failed to parse journal event due to invalid JSON syntax");
                                },
                                Category::Data => {
                                    if error.to_string().starts_with("unknown variant") {
                                        // we know the line is syntactically valid JSON if we got a
                                        // Data error, so we can safely extract the event type name
                                        let event_json: serde_json::Value = serde_json::from_str(&*line).unwrap();
                                        let event_type = &event_json["event"];
                                        info!(
                                            json = %line,
                                            "Ignored unimplemented event type {}",
                                            event_type,
                                        );
                                    } else {
                                        error!(
                                            json = %line,
                                            ?error,
                                            "Failed to parse journal event due to data not matching expected event structure",
                                        );
                                    }
                                },
                                Category::Eof => {
                                    error!(json = %line, ?error, "Failed to parse journal event due to unexpected end of input");
                                },
                            }
                        },
                    }
                },
                Err(error) => error!(current_line_number, ?error, "Failed to read new event from journal"),
            }
        }

        Ok(current_line_number)
    }
}
