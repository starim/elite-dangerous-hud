use std::borrow::Cow;
use std::fmt::{self, Display, Formatter};
use std::fs;
use std::path::{Path, PathBuf};

use serde::Deserialize;

use crate::error::Error;

#[derive(Debug, Clone, PartialEq, Deserialize)]
pub struct Config {
    game_data_dir: String,
    game_screenshots_dir: String,
    screenshots_target_dir: String,
    screenshot_target_format: String,
}

impl Config {
    pub fn new_from_file(path: &Path) -> Result<Self, Error> {
        let config_file_contents =
            fs::read_to_string(&path)
            .map_err(|error| {
                let message = format!(
                    "Error reading program config file ({})",
                    path.to_string_lossy(),
                );
                Error::new(Cow::from(message), Box::new(error))
            })?;

        toml::from_str(&*config_file_contents)
            .map_err(|error| {
                let message = "Unable to parse program config file to TOML";
                Error::new(Cow::from(message), Box::new(error))
            })
    }

    pub fn game_data_dir(&self) -> PathBuf {
        PathBuf::from(&*self.game_data_dir)
    }

    pub fn game_screenshots_dir(&self) -> PathBuf {
        PathBuf::from(&*self.game_screenshots_dir)
    }

    pub fn screenshots_target_dir(&self) -> PathBuf {
        PathBuf::from(&*self.screenshots_target_dir)
    }

    pub fn screenshot_target_format(&self) -> &str {
        &*self.screenshot_target_format
    }
}

impl Display for Config {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(
            f,
            concat!(
                "game_data_dir: {}\n",
                "screenshots_output_dir: {}\n",
                "screenshots_target_dir: {}\n",
                "screenshot_target_format: {}",
            ),
            self.game_data_dir,
            self.game_screenshots_dir,
            self.screenshots_target_dir,
            self.screenshot_target_format,
        )
    }
}
