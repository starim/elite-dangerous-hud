* use [std::env::current_exe](https://doc.rust-lang.org/1.12.1/std/env/fn.current_exe.html) to find files used at runtime, like the config file
	* or Cargo build scripts?

Add data sources:
* [Elite Dangerous Star Map (EDSM) System API](https://www.edsm.net/en/api-system-v1)
* [EDAstro POI API](https://edastro.com/poi/APIinfo)
* [Spansh data dumps](https://spansh.co.uk/dumps)
