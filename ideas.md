* warn if not ready to launch
	* no assigned crew
	* mission cargo not loaded
	* no limpets
* navigation
	* number of remaining jumps and star types on the way
		* calculate fuel per jump from [here](https://elite-dangerous.fandom.com/wiki/Frame_Shift_Drive#Hyperspace_Fuel_Equation)
		* mark when to refuel
		* warn if fuel will run out before a refuelable star or the destination is reached
* organizing missions
	* show what in the current system is relevant to my missions
	* or, if that's not possible, show the missions I have that correspond to the current system
	* show a map of my mission locations overlaid on the galaxy map
* security level of the system I'm in
* exploration:
	* when targeting a system on the galaxy map, display info on it
		* scoopable stars, and distance to them from the main star
		* POI
		* system scan value
	* is a system interesting?
		* when the DSS scanner runs, display how many bodies there are in the system
		* stars close together
		* landable planets close to a star
		* rare star types
		* notable stellar phenoma
		* particularly rapidly spinning planets
		* landable planets close to rings
		* landable planets close to or in rings
		* stations in rings
		* ringed stars
		* since Odyssey makes it hard to see planet types at a glance, list planet types or alert when high-value planet types are around
		* here's what the Elite Observatory add-on considers interesting (and [here is how they're defined](https://github.com/Xjph/ObservatoryCore/blob/4c1031b8f987872e6f2a17c5109ceadfca1d5a25/ObservatoryExplorer/DefaultCriteria.cs)):
			* Landable with oxygen atmosphere and rings
			* Landable large (>18000km radius)
			* Tiny objects (<300km radius)
			* Orbiting close to parent body
			* Shepherd moons (orbiting closer than a ring)
			* Close binary pairs
			* Colliding binary pairs
			* Moons of moons
			* Fast and non-locked rotation
			* Fast orbits
			* High eccentricity
			* Wide rings
			* Good jumponium availability (5/6 materials on a single body)
			* Full jumponium availability within a single system
			* Full jumponium availability on a single body (pretty sure this can't happen, but I'm watching for it anyway)
	* on mapping a planet
		* notify if any special locations are present--I nearly missed a crashed ship on one planet and only noticed by panning over its location marker by accident
	* on entering a new system
		* need to use FSS?
		* any first to map bodies present?
* mining display
	* show system reserves
	* show ring types
	* show hotspot types in scanned rings

